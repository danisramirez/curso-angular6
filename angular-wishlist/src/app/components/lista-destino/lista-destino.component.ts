import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje} from './../../models/destino-viaje.model';
import { DestinosApiClient} from './../../models/destino-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction} from './../../models/destinos-viajes-state.model';


@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  //destinos: DestinoViaje[];
  updates: string[];

  constructor(public destinoApiClient:DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    /*
    this.destinoApiClient.subscribedOnChange((d: DestinoViaje) => {
      if(d != null){
        this.updates.push('Se ha elegido a ' + d.nombre);
      }

    })
    */
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if(d != null){
          this.updates.push('Se ha elegido a ' + d.nombre);
        }
      });
   }

  ngOnInit(): void {
  }

  /*
  guardar(nombre:string, url:string):boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
    //console.log(new DestinoViaje(nombre,url));
    //console.log(this.destinos);
    return false;
  }*/
  agregado(d: DestinoViaje){
    this.destinoApiClient.add(d);
    this.onItemAdded.emit(d);	
    this.store.dispatch( new NuevoDestinoAction(d))
  }

  elegido(e: DestinoViaje){
    //this.destinos.forEach(function (x) {x.setSelected(false);});
    //d.setSelected(true);
    //this.destinoApiClient.getAll().forEach(x => x.setSelected(false));
    //e.setSelected(true);

    this.destinoApiClient.elegir(e);
    this.store.dispatch( new ElegidoFavoritoAction(e))
  }

}
