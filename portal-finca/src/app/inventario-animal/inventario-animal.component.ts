import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { InventarioAnimal} from './../models/inventario-animal.model';

@Component({
  selector: 'app-inventario-animal',
  templateUrl: './inventario-animal.component.html',
  styleUrls: ['./inventario-animal.component.css']
})
export class InventarioAnimalComponent implements OnInit {

  @Input() inventario: InventarioAnimal;
  @HostBinding('attr.class') cssClass='col-md-4';

  constructor() { }

  ngOnInit(): void {
  }

}
