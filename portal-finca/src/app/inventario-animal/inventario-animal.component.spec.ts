import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InventarioAnimalComponent } from './inventario-animal.component';

describe('InventarioAnimalComponent', () => {
  let component: InventarioAnimalComponent;
  let fixture: ComponentFixture<InventarioAnimalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InventarioAnimalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InventarioAnimalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
