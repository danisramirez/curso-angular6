export class InventarioAnimal {
	nombre: string;
	sexo: string;
	color: string;

	constructor(n:string, s:string, c:string){
		this.nombre = n;
		this.sexo = s;
		this.color = c;
	}
}