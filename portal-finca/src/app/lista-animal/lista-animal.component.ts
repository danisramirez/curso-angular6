import { Component, OnInit } from '@angular/core';
import { InventarioAnimal} from './../models/inventario-animal.model';

@Component({
  selector: 'app-lista-animal',
  templateUrl: './lista-animal.component.html',
  styleUrls: ['./lista-animal.component.css']
})
export class ListaAnimalComponent implements OnInit {
  inventarios: InventarioAnimal[];

  constructor() { 
  	this.inventarios = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, sexo:string, color:string):boolean{
  	this.inventarios.push(new InventarioAnimal(nombre, sexo, color));
	console.log(this.inventarios);
  	return false;
  }

}
